<h1 align="center">OASCI</h1>

## Install dependencies

Install all the dependencies using the following command.

```bash
npm install
```

## Development command

Start the development server using the following command.

```bash
npm run dev
```

## License

Code contained in this project is released under the [MIT license][mit] as specified in `LICENSE.md`.
All other data, information, documentation, and associated content provided within this project are released under the [CC BY 4.0 license][cc-by-4.0] as specified in `LICENSE_INFO.md`.

[mit]: https://spdx.org/licenses/MIT.html
[cc-by-4.0]: https://creativecommons.org/licenses/by/4.0/
