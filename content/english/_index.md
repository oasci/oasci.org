---
# Banner
banner:
  title: "open academic science"
  content: "A people-first approach to academic research and education."
  # image: "/img/branding/oasci-logo.svg"
features:
  - title: Transparency
    image: "/img/values/transparency.svg"
    caption: "<div style='opacity: 0.3'>Image by storyset on Freepik</div>"
    content: "We embrace openness and honesty in all aspects of our work.
    Our commitment to transparency ensures that you are informed, engaged, and empowered to contribute meaningfully to the scientific community."
  - title: Humility
    image: "/img/values/humility.svg"
    caption: "<div style='opacity: 0.3'>Image by pch.vector on Freepik</div>"
    content: "We strive to cultivate a humility-centric philosophy that rejects self-promotion and ego-driven motivations.
    Our members are united by a shared dedication to collaborative success; emphasizing collective achievement over individual acclaim."
  - title: Innovation
    image: "/img/values/innovation.svg"
    caption: "<div style='opacity: 0.3'>Image by Freepik</div>"
    content: "We dedicate ourselves to fostering innovation in both research and educational approaches.
    We actively encourage creativity and high-risk exploration of new ideas, positioning our organization at the forefront of forward-thinking initiatives."
  - title: Inclusivity
    image: "/img/values/inclusivity.png"
    caption: "<div style='opacity: 0.3'>Image by upklyak on Freepik</div>"
    content: "We prioritize a commitment to fostering an inclusive environment that warmly embraces diverse perspectives, backgrounds, and experiences. Our dedication ensures that every member of our community feels genuinely valued and respected."
---
