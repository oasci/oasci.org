---
title: Alex Maldonado
email: alex.maldonado@pitt.edu
image: "/img/people/alex-maldonado-square.jpg"
description: this is meta description
social:
  - name: github
    icon: fa-brands fa-github
    link: https://github.com/aalexmmaldonado

  - name: linkedin
    icon: fa-brands fa-linkedin
    link: https://www.linkedin.com/in/aalexmmaldonado/
---

Computational Biology Postdoc at [Pitt][pitt]

[pitt]: https://www.pitt.edu
